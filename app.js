const cookieParser = require('cookie-parser')
const express = require('express')
const session = require('express-session')
const passport = require('passport')
const bodyParser = require('body-parser')
const path = require('path')
const flash = require('connect-flash')
const app = express();
const MemoryStore = session.MemoryStore;
//const config = require("./configuracion")

const materiaRouter = require('./routes/materia')
const primerRouter = require('./routes/4materia')
const segundoRouter = require('./routes/5materia')
const tercerRouter = require('./routes/6materia')

//config.configuracionInicial()


app.set('views', path.join(__dirname, 'views'))
app.use(express.static(path.join(__dirname, 'resources/')))
///RUTAS
app.use('/materia', materiaRouter)
app.use('/primer', primerRouter)
app.use('/segundo', segundoRouter)
app.use('/tercer', tercerRouter)

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())
app.use(cookieParser())

// Configuraci�n de passport para sesiones

app.use(session({
    secret: 'pesecret',
    resave: true,
    saveUninitialized: true,
    store: new MemoryStore(),
}))

app.use(passport.initialize())
app.use(passport.session())
app.use(flash())



// Luego se podr� acceder a req.user (debe ir luego de initialize y session)
app.use((req, res, next) => {
    res.locals.user = req.user || null
    res.locals.messages = require('express-messages')(req, res)();
    next()
})



app.get('/', (req, res) =>{
    res.writeHead(200, {'Content-Type': 'text/plain'});
    res.end("Hola APP de HOrarios");
})

app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
})
module.exports = app
